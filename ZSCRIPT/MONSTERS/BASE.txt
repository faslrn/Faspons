class PlasmaDeath : Inventory {
	Default {
		inventory.maxamount 1;
	}
}

class ExDeath : Inventory {
	Default {
		inventory.maxamount 1;
	}
}

class BuckDeath : Inventory {
	Default {
		inventory.maxamount 1;
	}
}

class HSDeath : Inventory {
	Default {
		inventory.maxamount 1;
	}
}

class BehaviourCheckDone : Inventory {
	Default {
		Inventory.MaxAmount 1;
	}
}

class NoFearHere : Inventory {
	Default {
		Inventory.MaxAmount 1;
	}
}

class FearofMonster : Inventory {
	Default {
		Inventory.MaxAmount 1;
	}
}

class IsCharging : Inventory {
  Default {
    Inventory.MaxAmount 1;
  }
}

class GotEm : Inventory {
  Default {
    Inventory.MaxAmount 1;
  }
}
