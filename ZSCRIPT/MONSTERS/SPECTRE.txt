class SpectreRemix : DemonRemix {
	Default {
		+SHADOW;
		RenderStyle "Fuzzy";
		SeeSound "spectre/sight";
		AttackSound "spectre/melee";
		PainSound "spectre/pain";
		DeathSound "spectre/death";
		ActiveSound "spectre/active";
		HitObituary "$OB_SPECTREHIT";
	}
}
